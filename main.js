var http = require('http')
var express = require("express");
var app = express();
var router = express.Router();

router.get("/",function(req,res){
    res.type('application/json')
    res.end(JSON.stringify({response: 'Hello World'}))
});

app.use("/",router);
app.use("*",function(req,res){
  res.end("Not Found");
});
app.listen(3000,'0.0.0.0');